import 'react-native-gesture-handler';
import React from 'react';
import { View , Text } from 'react-native'
import { DefaultTheme,Provider as PaperProvider } from 'react-native-paper';
import Splash from './src/component/Splash';
import RootNavigation from './src/navigations/RootNavigation';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import { color } from "./src/resource/color";


const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: color.theme,
  },
};

export default class App extends React.Component{
  constructor(props){
     super(props);
     this.state = {
       visible : true
     }
   }
   componentDidMount(){
     this.checkLogin();
     setTimeout(()=>{
       this.hideSplashScreen()
     },5000)
    
   }

   hideSplashScreen = () => {
     this.setState({
       visible : false
     })
   }

   checkLogin = async() =>{
     const user = await AsyncStorage.getItem('user_id')
     this.setState({user})
     console.log("user",this.state.user)
   }
   render(){
     return(
      <PaperProvider theme={theme}>
        {this.state.visible ?
          (
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Splash />
            </View>
          )
          : (
            <NavigationContainer>
              <RootNavigation user={this.state.user}/>
            </NavigationContainer>
          )
        }
        
       </PaperProvider>
     )
   }
 }
