import React from 'react';
import { View , Text } from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper';
import Splash from './component/Splash';
import RootNavigation from './navigations/RootNavigation';

export default class Entry extends React.Component{
  constructor(props){
     super(props);
     this.state = {
       visible : true
     }
   }
   componentDidMount(){
     setTimeout(()=>{
       this.hideSplashScreen()
     },5000)
   }

   hideSplashScreen = () => {
     this.setState({
       visible : false
     })
   }
   render(){
     return(
       <View >
          {this.state.visible 
         ? <Splash />  
         : 
            <RootNavigation />
          } 
        
       </View> 
     )
   }
 }
