import React from 'react';
import { StyleSheet , View , Text , Image} from 'react-native'

export default class Splash extends React.Component{
  render(){
    return(
      <View style={styles.container}>
          {/* <Image source={require("../resource/image/flipcart.png")}
            style={styles.img}/>   */}
          <Image source={require("../resource/image/flipcart.png")}
            style={styles.img}/> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container : {
        //  flex:1,
        //  justifyContent : "center",
        //  alignItems:"center",
         //backgroundColor : '#047BD5'
    },
    img : {
        // width : "100%",
        // height : "100%",
        resizeMode : "cover"
    }
})