import React from 'react';
import { StyleSheet , View , Text , Image} from 'react-native'
import { color } from '../resource/color';

export default AppLogo =() => {
  
    return(
      <View style={styles.container}>
          <Image source={require("../resource/image/flipkart-logo.png")}
            style={styles.img}/>  
      </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex:1,
        // justifyContent : "center",
        alignItems:"center",
        
    },
    img : {
        width : 200,
        height : 50,
    }
})