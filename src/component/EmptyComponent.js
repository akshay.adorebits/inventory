import React from 'react';
import { StyleSheet , View , Text , Image} from 'react-native'
import { color } from '../resource/color';

export default EmptyComponent =(props) => {
  
    return(
        <View style={styles.emptyContainer}>
		    <Text style={styles.emptyText}>{props.title}</Text>
	    </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex:1,
        justifyContent : "center",
        alignItems:"center",
        
    },
    emptyContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 300
	},
	emptyText: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		fontSize: 18
	},
})