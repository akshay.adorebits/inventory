import React from 'react';
import { StyleSheet, View, Text, Image, SafeAreaView, Dimensions, TouchableHighlight } from 'react-native'
import AppLogo from '../component/AppLogo';
import { TextInput, Button } from 'react-native-paper';
import { color } from '../resource/color';
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-community/async-storage';

export default class LoginScreen extends React.Component {
  state = {
    mobile: '',
    password: ''
  }

  handleSubmit() {
    var mobile = this.state.mobile;
    var password = this.state.password;

    if (mobile == '') {
      alert("Enter mobile number")
    } else if(mobile.length <10){
      alert("Mobile number should be 10 digit.")
    } else if (password == '') {
      alert("Enter password")
    } else {
      //loginUser( mobile, password )
      const db = openDatabase({ name: 'UserDatabase.db' });
      console.log(db)
      db.transaction((txn) => {
        console.log(txn)
        try {
         
            txn.executeSql(
                "CREATE TABLE IF NOT EXISTS user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20), mobile INT(10) UNIQUE, password VARCHAR(255))"
                ,[])
            console.log("Table Created")
           // txn.executeSql('DROP TABLE IF EXISTS user', []);
        
         txn.executeSql("SELECT * FROM user WHERE mobile='" + mobile + "' AND password='" + password + "' ", [], (tx, result) => {
          console.log(result.rows)
          var len = result.rows.length;
          if (len > 0) {
            console.log("response", result.rows.item(0))
            //return result.rows.item(0);
            this.userData(result.rows.item(0));
          }else{
            alert("Mobile number or password is wrong")
            this.setState({
              mobile: '',
              password: ''
            })
          }
        })
      }catch(err){
        alert("User is not exist")
      }
      })

      this.setState({
        mobile: '',
        password: ''
      })
    }
  }

  userData = async (res) => {
    
    await AsyncStorage.setItem( 'name', res.name )
    await AsyncStorage.setItem( 'mobile', JSON.stringify(res.mobile) )
    await AsyncStorage.setItem( 'user_id', JSON.stringify(res.user_id) )
    console.log(res)
    if(res.user_id == '1'){
      this.props.navigation.navigate('Admin')
    }else{
      this.props.navigation.navigate('Client')
    }
    
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.logo}>
          <AppLogo />
        </View>
        <View style={styles.heading}>
          <Text style={styles.headingTxt}>Login to check your account</Text>
        </View>
        <View style={styles.form}>
          <TextInput
            label="Mobile"
            mode='outlined'
            keyboardType="number-pad"
            onChangeText={(mobile) => this.setState({ mobile })}
            value={this.state.mobile}
            style={styles.textbox}
            maxLength={10}
          />
          <TextInput
            label="Password"
            mode='outlined'
            secureTextEntry
            onChangeText={(password) => this.setState({ password })}
            style={styles.textbox}
            value={this.state.password}
          />
          <Button
            mode="contained"
            style={styles.button}
            onPress={() => this.handleSubmit()}>
            <Text style={styles.btntxt}>Login</Text>
          </Button>
        </View>
        <View style={styles.signContainer}>
          <Text style={styles.signtxt}>Create new user ?</Text>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={styles.signtxt1}> Register</Text>
          </TouchableHighlight>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    // alignItems:'center'
  },
  logo: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  heading: {
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 40
  },
  headingTxt: {
    fontSize: 20,
    fontWeight: "700"
  },
  form: {

  },
  textbox: {
    width: Dimensions.get('window').width - 30,
    marginHorizontal: 10,
    marginVertical: 10,
    borderColor: color.theme
  },
  button: {
    width: Dimensions.get('window').width - 20,
    height: 50,
    marginHorizontal: 10,
    marginVertical: 10,
    paddingTop: 6,
    backgroundColor: color.theme
  },
  btntxt: {
    fontSize: 18,
    fontWeight: "700",
    borderRadius: 5,
    color : color.primery
  },
  signContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  signtxt: {
    fontSize: 18,
    //textAlign : 'center'
  },
  signtxt1: {
    fontSize: 18,
    color: color.theme
  }
})