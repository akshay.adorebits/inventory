import React from 'react';
import { StyleSheet, View, Text, Image, SafeAreaView, Dimensions, TouchableHighlight } from 'react-native'
import { TextInput, Button } from 'react-native-paper';
import { color } from '../../resource/color';
import { openDatabase } from 'react-native-sqlite-storage';
import {createTableProduct,updateProduct} from '../../action/appAction';
import ImagePicker from 'react-native-image-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class AddProduct extends React.Component {
    state = {
        pro_id:'',
        productName: '',
        qty: '',
        price:''
    }

    componentDidMount(){
        if(this.props.route.params){
            // const { id } = this.props.route.params;
             
            const { product } = this.props.route.params;
            
            this.setState({pro_id : product.pro_id})
            this.setState({productName : product.pro_name})
            this.setState({qty : product.qty.toString()})
            this.setState({price : product.price.toString()})
            this.setState({filePath : product.image})
            
            
        }
    }

    checkQty(qty){
        if(qty == '-' || qty == " "){
            alert("invalid input");
        }else{
            this.setState({qty})
        }
    }

    checkPrice(price){
        if(price == '-' || price == " "){
            alert("invalid input");
        }else{
            this.setState({price})
        }
    }

    imagePicker(){
        
       /* var options = {
            title: 'Select Image',
            // customButtons: [
            //   { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            // ],
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);
      
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
              alert(response.customButton);
            } else {
              let source = response.data;
              // You can also display the image using data:
              // let source = { uri: 'data:image/jpeg;base64,' + response.data };
              
              var imageName = response.uri.substring(response.uri.lastIndexOf('/')+1);
              
              this.setState({
                filePath: source,
                imgName : imageName
              });
            }
        });*/

        const options = {
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            base64: true
        }

        ImagePicker.launchImageLibrary(options, response => {
            if (response.uri) {
                var imageName = response.uri.substring(response.uri.lastIndexOf('/') + 1)
              this.setState({ filePath: response.data,
                imgName : imageName })
              console.log("image",response.data)
              console.log("name",response.uri.substring(response.uri.lastIndexOf('/') + 1))
              alert(response.uri.substring(response.uri.lastIndexOf('/') + 1))
            }
        })

    }
    handleSubmit(){
        var productName = this.state.productName;
        var qty = this.state.qty;
        var price = this.state.price;
        var image = this.state.filePath;

        if (productName == '') {
            alert("Enter product name")
          } else if (qty == '') {
            alert("Enter quantity")
          } else if (price == '') {
            alert("Enter price")
          } else {
            if(this.state.pro_id == ''){
                createTableProduct (productName,qty,price,image) 
                this.setState({
                    productName: '',
                    qty: '',
                    price:'',
                    imgName:'',
                    filePath:''
                })
            }else{
                
                var pro_id = this.state.pro_id;
                var image = this.state.filePath;

                var navigation = this.props.navigation;
                 if(image){
                    updateProduct(pro_id,productName,qty,price,image,navigation)
                 }
                
            }
            
          }
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.form}>
                    <TextInput
                        label="Product Name"
                        mode='outlined'
                        onChangeText={(productName) => this.setState({ productName })}
                        value={this.state.productName}
                        style={styles.textbox}
                    />
                    <TextInput
                        label="Quantity"
                        mode='outlined'
                        keyboardType="number-pad"
                        onChangeText={(qty) => this.checkQty(qty)}
                        style={styles.textbox}
                        value={this.state.qty}
                    />
                    <TextInput
                        label="Price"
                        mode='outlined'
                        keyboardType="number-pad"
                        onChangeText={(price) => this.checkPrice(price)}
                        style={styles.textbox}
                        value={this.state.price}
                    />
                    <View style={styles.picker}>
                        <TouchableOpacity
                            style={styles.pin}
                            onPress={()=>this.imagePicker()}>
                            <Image source={require('../../resource/image/icons/Attached.png')} style={styles.imgpin}
                            />
                        </TouchableOpacity>
                         <Text>{this.state.imgName}</Text>
                    </View>
                    <Button
                        mode="contained"
                        style={styles.button}
                        onPress={() => this.handleSubmit()}>
                        
                        <Text style={styles.btntxt}>
                        {this.state.pro_id ?
                            'Update Product'
                            :   
                            'Add Product' }</Text>
                    </Button>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent:'center',
        // alignItems:'center'
    },
    logo: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    heading: {
        marginHorizontal: 10,
        marginTop: 20,
        marginBottom: 40
    },
    headingTxt: {
        fontSize: 20,
        fontWeight: "700"
    },
    form: {

    },
    textbox: {
        width: Dimensions.get('window').width - 30,
        marginHorizontal: 10,
        marginVertical: 10,
        borderColor: color.theme
    },
    button: {
        width: Dimensions.get('window').width - 20,
        height: 50,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingTop: 6,
        backgroundColor: color.theme
    },
    btntxt: {
        fontSize: 18,
        fontWeight: "700",
        borderRadius: 5,
        color : color.primery
    },
    signContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    signtxt: {
        fontSize: 18,
        //textAlign : 'center'
    },
    signtxt1: {
        fontSize: 18,
        color: color.theme
    },
    picker :{
        flexDirection : 'row',
        borderWidth:1,
        borderColor : color.theme,
        marginHorizontal:15,
        borderRadius:5,
        paddingVertical:12,
        marginTop:10
    },
    imgpin :{
        width : 30,
        height:30
    },
    pin : {
        borderRightWidth : 1,
        borderRightColor : color.theme,
        paddingHorizontal : 10
    }
})

