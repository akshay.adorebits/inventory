import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class ProfileScreen extends React.Component {
    state = {
        name : '',
        mobile : ''
    }
    componentDidMount(){
        this.getUserData();
    }

    getUserData = async() =>{
        await AsyncStorage.getItem('name').then((name)=>{
            this.setState({name : name})
        })
        await AsyncStorage.getItem('mobile').then((mobile)=>{
            this.setState({mobile : mobile})
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}></View>
                <Image style={styles.avatar} source={ require('../../resource/image/avatar6.png')} />
                <View style={styles.body}>
                    <View style={styles.bodyContent}>
                        <Text style={styles.name}>{this.state.name}</Text>
                            <Text style={styles.info}>UX Designer / Mobile developer - {this.state.mobile}</Text>
                        <Text style={styles.description}>Lorem ipsum dolor sit amet, saepe sapientem eu nam. Qui ne assum electram expetendis, omittam deseruisse consequuntur ius an,</Text>

                        {/* <TouchableOpacity style={styles.buttonContainer}>
                            <Text>Opcion 1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonContainer}>
                            <Text>Opcion 2</Text>
                        </TouchableOpacity> */}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
       flex:1,
       //justifyContent : 'center',
       alignItems : 'center'
    },  
    header:{
        backgroundColor: "#00BFFF",
        height:200,
      },
      avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom:10,
        alignSelf:'center',
        position: 'absolute',
        marginTop:130
      },
      body:{
        marginTop : 100
      },
      bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding:30,
      },
      name:{
        fontSize:28,
        color: "black",
        fontWeight: "600"
      },
      info:{
        fontSize:16,
        color: "black",
        marginTop:10
      },
      description:{
        fontSize:16,
        color: "#696969",
        marginTop:10,
        textAlign: 'center'
      },
      buttonContainer: {
        marginTop:10,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#00BFFF",
      },
})