import React from 'react';
import { View, Text, StyleSheet,FlatList,Dimensions,TouchableOpacity,RefreshControl,Image} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-community/async-storage';
import { acceptOrder,cancleOrder } from '../../action/appAction';
import EmptyComponent from '../../component/EmptyComponent';

export default class ConfirmOrder extends React.Component{
    state = {
        data : [],
        isRefreshing : false
    }

    constructor(props){
        super(props)
        //this.productData();
    }
    componentDidMount(){
        const { navigation } = this.props;
		
			//this.productData();

            this._unsubscribe = navigation.addListener('focus', () => {
                this.ConfinOrder();
            });
        
    }

    componentWillUnmount() {
		// Remove the event listener
        //this.focusListener.remove();
        this._unsubscribe();
    }
    
    ConfinOrder = async() =>{

        const db = openDatabase({ name: 'UserDatabase.db' });
        console.log(db)
        db.transaction((txn) => {
            txn.executeSql("SELECT cart.order_id,cart.pro_id,cart.pro_name,cart.qty,cart.price,product.qty as total_qty,cart.user_name,cart.user_id,cart.status,product.image as image FROM cart JOIN product ON cart.pro_id = product.pro_id where status = '1'", [], (tx, result) => {
                var temp = [];
                for (let i = 0; i < result.rows.length; ++i)
                temp.push(result.rows.item(i));
                this.setState({data:temp})

                console.log("data ==>",this.state.data)
            })
        })
    }

    onRefresh() {
        this.ConfinOrder();
    }

    acceptOrder(id){
        acceptOrder(id)
        this.onRefresh();
    }

    cancleOrder(id,pro_id){
        cancleOrder(id,pro_id)
        this.onRefresh();
    }
    _renderItem = ({ item, index }) => {
        
        return (
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff',paddingHorizontal:10,paddingVertical:10, marginBottom: 5,  height: 130, marginRight: 5,width:Dimensions.get('window').width-10 }}>
                <View>
                        <Image source={{ uri: 'data:image/jpeg;base64,' + item.image }} style={[styles.centerElement, { height: 70, width: 70, backgroundColor: '#eeeeee' }]} />
                    </View>
                <View style={{ flexGrow: 1, paddingRight: 5 }}>

                    <Text style={{ fontSize: 18, fontWeight:"700", textTransform:"uppercase"}}>{item.pro_name}</Text>

                    <Text style={{ fontSize: 16, marginVertical:3, textTransform:"uppercase"}}>Customer : {item.user_name}</Text>
                    <View style={{flexDirection:"row"}}>
                        <Text>Price : {item.price}</Text>
                        <Text style={{marginLeft : 50}}>Quantity : {item.qty}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        {item.status == 0 ?
                        <TouchableOpacity style={{marginTop : 10}}
                           onPress={()=>this.acceptOrder(item.order_id)} >
                            <Text style={{color:'blue',fontSize:17}}>Accept</Text>
                        </TouchableOpacity>
                        : item.status == 3 ? <Text style={{color:'gray',fontSize:17,marginTop:10}}>Cancel Order</Text> : <Text style={{color:'gray',fontSize:17,marginTop:10}}>Confirm</Text>} 
                        {item.status == 0 ?
                        <TouchableOpacity style={{marginTop : 10,marginLeft :30}}
                            onPress={()=>this.cancleOrder(item.order_id,item.pro_id)}>
                            <Text style={{color:'blue',fontSize:17}}>Cancle</Text>
                        </TouchableOpacity>
                        : null }
                    </View>
                </View>
                
            </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={item => item.pro_id.toString()}
                    extraData={this.state}
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={()=>this.onRefresh()}
                        />
                    }
                    ListEmptyComponent={
                        <EmptyComponent title="Data not available." />
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        justifyContent : 'center',
        alignItems : 'center'
    },
    centerElement: { 
        justifyContent: 'center', 
        alignItems: 'center' ,
        marginRight : 20
    
    },
})