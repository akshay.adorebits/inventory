import React from 'react';
import { View, Text, Image,StyleSheet, FlatList, Dimensions, TouchableOpacity, RefreshControl, Alert,TouchableHighlight } from 'react-native';
import { color } from '../../resource/color';
import { openDatabase } from 'react-native-sqlite-storage';
import { deleteProduct } from '../../action/appAction';
import EmptyComponent from '../../component/EmptyComponent';


class HomeScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            delete_pro_id:'',
            isRefreshing: false,
            modalVisible: false,
            modalCancle: false,
        }
        console.log("Constructor call")
        this.productData();
    }

    componentDidMount() {
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.productData();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    openModal(pro_id) {
		// this.setState({ delete_pro_id: pro_id });
        // this.setState({ modalCancle: true });
        Alert.alert(
            'Are you sure you want to delete this item ?',
            '',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Delete', onPress: () => { 
                        this.deleteProduct(pro_id)
                    }
                },
            ],
            { cancelable: false }
        );
    }

    productData() {
        const db = openDatabase({ name: 'UserDatabase.db' });
        console.log(db)
        db.transaction((txn) => {
            txn.executeSql("SELECT * FROM product ", [], (tx, result) => {
                var temp = [];
                for (let i = 0; i < result.rows.length; ++i)
                    temp.push(result.rows.item(i));
                    this.setState({ data: temp })

                console.log("data ==>", this.state.data)
            })
        })
    }

    deleteProduct(id) {
        var navigation = this.props.navigation;
        deleteProduct(id, navigation)
        this.setState({ modalCancle: false });
       this.onRefresh();
    }

    onRefresh() {
        this.productData();
    }

    _renderItem = ({ item, index }) => {

        return (
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff', paddingHorizontal: 10, paddingVertical: 10, marginBottom: 5, height: 100, marginLeft: 5, width: Dimensions.get('window').width - 10 }}>
                <View>
                <Image source={{ uri: 'data:image/jpeg;base64,' + item.image }} style={[styles.centerElement, { height: 70, width: 70, backgroundColor: '#eeeeee' }]} />
                </View>
                <View style={{ flexGrow: 1, paddingRight: 5 }}>
                    <Text style={{ fontSize: 18, fontWeight: "700", textTransform: "uppercase" }}>{item.pro_name}</Text>
                    <View style={{ flexDirection: "row" }}>
                        <Text>Price : {item.price}</Text>
                        <Text style={{ marginLeft: 50 }}>Quantity : {item.qty}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ marginTop: 10 }}
                            onPress={() => this.props.navigation.navigate('Product', { product: item, edit: true })}>
                            <Text style={{ color: 'blue', fontSize: 17 }}>Edit</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginTop: 10, marginLeft: 20 }}
                            //onPress={() => this.deleteProduct(item.pro_id)}
                            onPress={()=>this.openModal(item.pro_id)}
                            >
                            <Text style={{ color: 'blue', fontSize: 17 }}>Delete</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        )
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={item => item.pro_id.toString()}
                    extraData={this.state}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }
                    ListEmptyComponent={
                        <EmptyComponent title="Data not available." />
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerElement: { 
        justifyContent: 'center', 
        alignItems: 'center' ,
        marginRight : 20
    
    },
})

export default HomeScreen;