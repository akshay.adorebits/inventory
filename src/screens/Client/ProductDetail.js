import React from 'react'
import {View ,TouchableHighlight,TouchableOpacity, StyleSheet, Text, TextInput, Image, ScrollView, ToastAndroid } from 'react-native'
import { color } from '../../resource/color'
import AsyncStorage from '@react-native-community/async-storage';
import {Order} from '../../action/appAction';

export default class ProductDetail extends React.Component {
    state = {
        quantity: "1"
    }


    handleAddCart = async(product) => {
        const quantity = parseInt(this.state.quantity)
        var navigation = this.props.navigation;

        var name = await AsyncStorage.getItem( 'name')
        var mobile = await AsyncStorage.getItem( 'mobile')
        var user_id = await AsyncStorage.getItem( 'user_id')

        Order(product.pro_id,product.pro_name,quantity,product.price,user_id,name,mobile,navigation);

    }

    _increment = (qty) => {
        const result = parseInt(this.state.quantity) + 1
        if(result <= qty){
            this.setState({
                quantity: result.toString()
            })
        }
        
    }

    _decrement = () => {
        const result = parseInt(this.state.quantity) - 1

        if(result > 0){
            this.setState({
                quantity: result.toString()
            })
        }
        
    }

    render() {
        
        const { product } = this.props.route.params;
       
        return (
            <ScrollView contentContainerStyle={s.container}>
                <View style={s.productContainer}>
                    <Text style={s.proname}>{product.pro_name}</Text>
                </View>
                
                <Image style={s.proimage} source={{uri: 'data:image/jpeg;base64,' + product.image}} />
               
                <View style={s.priceContent}>
                    <Text style={s.price}>$ {product.price}</Text>
                </View>

                <Text style={{  lineHeight: 30 }}>{product.info}</Text>
                <View style={s.cartDetail}>
                    <TouchableOpacity onPress={() => this._decrement()} 
                        style={s.plus}>
                        <Image source={require('../../resource/image/icons/minus-big-512.png')} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                    <Text style={s.counter}>{ product.qty == 0 ? 0 :this.state.quantity}</Text>
                    <TouchableOpacity onPress={() => this._increment(product.qty)} style={s.minus}>
                        <Image source={require('../../resource/image/icons/plus.png')} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                </View>

                <View style={s.addCartContent}>
                    {product.qty == 0 ?
                    <View>
                        <Text style = {s.out}>Out of stock</Text>
                        <TouchableHighlight style={s.buttondisable} disabled={true}>
                            <Text style={s.btnTxt} >Add Cart</Text>
                        </TouchableHighlight> 
                    </View> :
                    <TouchableHighlight style={s.button} onPress={()=>this.handleAddCart(product)}>
                        <Text style={s.btnTxt} >Add Cart</Text>
                    </TouchableHighlight>
                    }
                </View>

            </ScrollView>
        )
    }
}


const s = StyleSheet.create({
    container: {
        marginTop: 50,
    //    flex : 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    productContainer :{
        
    },
    proname : {
        fontSize : 25,
        fontWeight : '700',
        textTransform:"uppercase"
    },
    proimage : {
        marginTop: 6, 
        marginBottom: 6, 
        width: 150, 
        height: 150
    },
    cartDetail :{
        flexDirection: 'row',
        marginTop : 50
    },
    plus : {
        borderWidth: 1, 
        borderColor: '#cccccc', 
        marginRight: 10
    },
    minus : {
         borderWidth: 1, 
         borderColor: '#cccccc', 
         marginLeft: 10 
    },
    counter : {
        borderTopWidth: 1, 
        borderBottomWidth: 1, 
        borderColor: '#cccccc', 
        paddingHorizontal: 7, 
        paddingTop: 3, 
        color: '#000000', 
        fontSize: 20 
    },
    priceContent:{
        marginTop : 50,

    },
    price:{
        fontSize : 20
    },
    addCartContent : {
        marginTop : 20
    },
    button :{ 
        margin: 10, 
        padding: 15, 
        borderWidth : 1,
        width : 300,
        backgroundColor : color.theme,
        borderColor : color.theme
    },
    buttondisable :{
        margin: 10, 
        padding: 15, 
        borderWidth : 1,
        width : 300,
        backgroundColor : color.cancel,
        borderColor : color.theme
    },
    btnTxt : {
        fontSize: 16, color: color.primery ,
        textAlign : 'center'
    },
    out : {
        color : 'red',
        textAlign : 'center',
        fontSize : 20
    }
})


