import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import { StyleSheet, Button, Text, View, TouchableOpacity, ScrollView, Image, ActivityIndicator, TextInput, Alert } from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import {deleteCart, updateCart} from '../../action/appAction';
import EmptyComponent from '../../component/EmptyComponent';

export default class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cartItems: [],
            cartItemsIsLoading: false,
        }

        this.cartHandler();
    }

    componentDidMount(){
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.cartHandler();
        });
    }

    componentWillUnmount(){
        this._unsubscribe();
    }

    cartHandler = async() =>{
        var user_id = await AsyncStorage.getItem('user_id')
      
        const db = openDatabase({ name: 'UserDatabase.db' });
       
        db.transaction((txn) => {
            txn.executeSql("SELECT cart.order_id,cart.pro_id,cart.pro_name,cart.qty,cart.price,product.qty as total_qty, product.image FROM cart JOIN product ON cart.pro_id = product.pro_id where cart.status='0' AND cart.user_id = '"+user_id+"'", [], (tx, result) => {
                
                if(result.rows.length >0){
                    var temp = [];
                    for (let i = 0; i < result.rows.length; ++i){
                        temp.push(result.rows.item(i));
                        this.setState({cartItems:temp})
                    }
                }else{
                    console.log("null")
                    this.setState({cartItems:null})
                }
                
                    console.log("cart data ======>",this.state.cartItems)
                })
        })
    }

    deleteHandler = (index) => {
        Alert.alert(
            'Are you sure you want to delete this item from your cart?',
            '',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Delete', onPress: () => {
                        this.deleteCart(index.pro_id,index.order_id)
                    }
                },
            ],
            { cancelable: false }
        );
    }

    deleteCart = (pro_id,order_id) => {
       deleteCart(pro_id,order_id)
       //alert(index)
       this.cartHandler();
    }

    quantityHandler = (action, index,total_qty) => {
        const newItems = [...this.state.cartItems];  

        let currentQty = newItems[index]['qty'];
       
        if (action == 'more') {
            
            if( currentQty < total_qty){
                newItems[index]['qty'] = currentQty + 1;
                newItems[index]['cart_change'] = 'more';
            }
            
        } else if (action == 'less') {
            newItems[index]['qty'] = currentQty > 1 ? currentQty - 1 : 1;
            newItems[index]['cart_change'] = 'less';
        } 

        this.setState({ cartItems: newItems }); // set new state
       // console.log("cart update",this.state.cartItems)
    }


    subtotalPrice = () => {
        const { cartItems } = this.state;
        if (cartItems) {
            return cartItems.reduce((sum, item) => sum + (item.qty * item.price ), 0);
        }
        return 0;
    }

    updateCart(cartItems){
        if(cartItems == null){
            alert("Cart is empty")
        }else{
            updateCart(cartItems)  
        }
       
    }

    render() {
        const styles = StyleSheet.create({
            centerElement: { justifyContent: 'center', alignItems: 'center' },
        });

        const { cartItems, cartItemsIsLoading} = this.state;
        
        return (
            <View style={{ flex: 1, backgroundColor: '#f6f6f6' }}>

                {cartItemsIsLoading ? (
                    <View style={[styles.centerElement, { height: 300 }]}>
                        <ActivityIndicator size="large" color="#ef5739" />
                    </View>
                ) : 
                     (cartItems ? 
                        (<ScrollView>
                            {cartItems && cartItems.map((item, i) => (
                                <View key={i} style={{ flexDirection: 'row', backgroundColor: '#fff', marginBottom: 2, height: 120 }}>
                                    <View style={{ flexDirection: 'row', flexGrow: 1, flexShrink: 1, alignSelf: 'center',paddingLeft:10 }}>
                                        <TouchableOpacity onPress={() => {/*this.props.navigation.navigate('ProductDetails', {productDetails: item})*/ }} style={{ paddingRight: 10 }}>
                                            <Image source={{ uri: 'data:image/jpeg;base64,' + item.image }} style={[styles.centerElement, { height: 60, width: 60, backgroundColor: '#eeeeee' }]} />
                                        </TouchableOpacity>
                                        <View style={{ flexGrow: 1, flexShrink: 1, alignSelf: 'center' }}>
                                            <Text numberOfLines={1} style={{ fontSize: 15 }}>{item.pro_name}</Text>
                                            <Text numberOfLines={1} style={{ color: '#8f8f8f' }}>{item.color ? 'Variation: ' + item.color : ''}</Text>
                                            <Text numberOfLines={1} style={{ color: '#333333', marginBottom: 10 }}>$ {item.qty * item.price}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <TouchableOpacity onPress={() => this.quantityHandler('less', i,item.total_qty)} style={{ borderWidth: 1, borderColor: '#cccccc',marginRight:3 }}>
                                                    <Image source={require('../../resource/image/icons/minus-big-512.png')} style={{width:20,height:20}}/>
                                                </TouchableOpacity>
                                                <Text style={{ borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#cccccc', paddingHorizontal: 7, paddingTop: 3, color: '#bbbbbb', fontSize: 13 }}>{item.qty}</Text>
                                                <TouchableOpacity onPress={() => this.quantityHandler('more', i,item.total_qty)} style={{ borderWidth: 1, borderColor: '#cccccc',marginLeft:5 }}>
                                                <Image source={require('../../resource/image/icons/plus.png')} style={{width:20,height:20}}/>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={[styles.centerElement, { width: 60 }]}>
                                        <TouchableOpacity style={[styles.centerElement, { width: 32, height: 32 }]} onPress={() => this.deleteHandler(item)}>
                                        <Image source={require('../../resource/image/icons/delete.png')} style={{width:50,height:50}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))}
                        </ScrollView>
                        ):(
                            <EmptyComponent title="Data not available." />
                        )
                    )  
                }

                {!cartItemsIsLoading  ?
                    (<View style={{ backgroundColor: '#fff', borderTopWidth: 2, borderColor: '#f6f6f6', paddingVertical: 5 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', height: 32, paddingRight: 20, alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', flexGrow: 1, flexShrink: 1, justifyContent: 'space-between', alignItems: 'center' }}>
                                
                                <View style={{ flexDirection: 'row', paddingRight: 20, alignItems: 'center' }}>
                                    <Text style={{ color: '#8f8f8f' }}>SubTotal: </Text>
                                    <Text>${this.subtotalPrice().toFixed(2)}</Text>
                                </View>
                            </View>

                            
                            <TouchableOpacity 
                                    style={[styles.centerElement, { backgroundColor: '#0faf9a', width: 100, height: 25, borderRadius: 5 }]} 
                                    onPress={() => this.updateCart(this.state.cartItems)}>
                                        <Text style={{ color: '#ffffff' }}>Checkout</Text>
                            </TouchableOpacity>
                           
                        </View>
                    </View>) :null
                }
            </View>
        );
    }
}