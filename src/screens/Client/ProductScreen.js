import React from 'react';
import { View, Text, StyleSheet,FlatList,Dimensions,TouchableOpacity,RefreshControl,Image} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import EmptyComponent from '../../component/EmptyComponent';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { color } from '../../resource/color';

export default class ProductScreen extends React.Component{
    state = {
        data : [],
        isRefreshing : false,
        count : 0
    }

    componentDidMount(){
        // this._unsubscribe = navigation.addListener('focus', () => {
        //     this.productData();
        // });
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.productData();
        });
    }

    componentWillUnmount() {
		// Remove the event listener
       
        this._unsubscribe();
    }

    productData(){
        const db = openDatabase({ name: 'UserDatabase.db' });
        console.log(db)
        db.transaction((txn) => {
            txn.executeSql("SELECT * FROM product ", [], (tx, result) => {
                var temp = [];
                for (let i = 0; i < result.rows.length; ++i)
                temp.push(result.rows.item(i));
                this.setState({data:temp})

            })
        })
    }

    onRefresh() {
        this.productData();
    }


    // addToCart = async(item) =>{

    //    if(this.state.count > 0){
    //         console.log("Cart",item)
    //         var name = await AsyncStorage.getItem( 'name')
    //         var mobile = await AsyncStorage.getItem( 'mobile')
    //         var user_id = await AsyncStorage.getItem( 'user_id')

    //         Order(item.pro_id,item.pro_name,this.state.count,item.price,user_id,name,mobile);
    //         this.onRefresh();
    //    }else{
    //        alert("please select perchase quantity")
    //    }
        
    // }

    _renderItem = ({ item, index }) => {
        
        return (
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('ProductDetail',{product: item})}>
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff',paddingHorizontal:10,paddingVertical:10, marginBottom: 5,  height: 90, marginRight: 5,width:Dimensions.get('window').width }}>
                <View>
                    <Image source={{ uri: 'data:image/jpeg;base64,' + item.image }} style={[styles.centerElement, { height: 70, width: 70, backgroundColor: '#eeeeee' }]} />
                </View>
                <View style={{ flexGrow: 1, paddingRight: 5 }}>
                    <Text style={{ fontSize: 18, fontWeight:"700", textTransform:"uppercase"}}>{item.pro_name}</Text>
                    <View style={{flexDirection:"row",marginTop:8}}>
                        <Text>Price : {item.price}</Text>
                        <Text style={{marginLeft : 30}}>Total Quantity : {item.qty}</Text>
                    </View>
                </View>
                
            </View>
        </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={item => item.pro_id.toString()}
                    extraData={this.state}
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={()=>this.onRefresh()}
                        />
                    }
                    ListEmptyComponent={
                        <EmptyComponent title="Data not available." />
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        justifyContent : 'center',
        alignItems : 'center'
    },
    centerElement: { 
        justifyContent: 'center', 
        alignItems: 'center' ,
        marginRight : 20
    
    },
})