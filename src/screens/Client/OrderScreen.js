import React from 'react';
import { View, Text, StyleSheet,FlatList,Dimensions,TouchableOpacity,RefreshControl,Image} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-community/async-storage';
import EmptyComponent from '../../component/EmptyComponent';

export default class OrderScreen extends React.Component{
    state = {
        data : [],
        isRefreshing : false
    }

    componentDidMount(){
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.productData();
        });
    }

    componentWillUnmount() {
		// Remove the event listener

        this._unsubscribe();
    }

    productData = async() =>{
        var user_id = await AsyncStorage.getItem('user_id');

        const db = openDatabase({ name: 'UserDatabase.db' });
        console.log(db)
        db.transaction((txn) => {
            txn.executeSql("SELECT cart.order_id,cart.pro_id,cart.pro_name,cart.qty,cart.price,product.qty as total_qty,cart.user_name,cart.user_id,cart.status, product.image as image FROM cart JOIN product ON cart.pro_id = product.pro_id WHERE user_id = '"+user_id+"' AND status !='0' ", [], (tx, result) => {
                var temp = [];
                for (let i = 0; i < result.rows.length; ++i)
                temp.push(result.rows.item(i));
                this.setState({data:temp})

                console.log("data ==>",this.state.data)
            })
        })
    }

    onRefresh() {
        this.productData();
    }
    _renderItem = ({ item, index }) => {

        return (
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff',paddingHorizontal:10,paddingVertical:10, marginBottom: 5,  height: 100, marginRight: 5,width:Dimensions.get('window').width-10 }}>
                <View>
                    <Image source={{ uri: 'data:image/jpeg;base64,' + item.image }} style={[styles.centerElement, { height: 70, width: 70, backgroundColor: '#eeeeee' }]} />
                </View>
                <View style={{ flexGrow: 1, paddingRight: 5 }}>
                    <Text style={{ fontSize: 18, fontWeight:"700", textTransform:"uppercase"}}>{item.pro_name}</Text>
                    <View style={{flexDirection:"row"}}>
                        <Text>Salary : {item.price}</Text>
                        <Text style={{marginLeft : 50}}>Qtuanty : {item.qty}</Text>
                    </View>
                    {/* <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{marginTop : 10}}
                            >
                            <Text style={{color:'blue',fontSize:17}}>Delete</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
                <View style={{justifyContent:'flex-end'}}>
                    <Text style={{fontSize : 18}}>
                        {item.status == 0 ? 'Not Confirm' : item.status == 3 ? 'Cancel Order' :'Confirm'}
                    </Text>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={item => item.pro_id.toString()}
                    extraData={this.state}
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={()=>this.onRefresh()}
                        />
                    }
                    ListEmptyComponent={
                        <EmptyComponent title="Data not available." />
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        justifyContent : 'center',
        alignItems : 'center'
    },
    centerElement: { 
        justifyContent: 'center', 
        alignItems: 'center' ,
        marginRight : 20
    
    },
})



