import React from 'react';
import {View,Text} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class Logout extends React.Component{

	constructor(props){
		super(props);
		console.log("logout call")
		this._signOutAsync();
		//props.navigation.navigate('Login')
	}

	
    UNSAFE_componentWillMount(){
		this._signOutAsync();
	}

	/** logout and remove user detail from storage */
	_signOutAsync = async () => {
		this.props.navigation.navigate('Login');

		await AsyncStorage.removeItem('user_id');
        await AsyncStorage.removeItem('name');
        await AsyncStorage.removeItem('mobile');
		
    };
    
    render(){
        return (
			<View>
			</View>
		);
    }
}