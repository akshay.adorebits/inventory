import React from 'react';
import { StyleSheet, View, Text, Image, SafeAreaView, Dimensions, TouchableHighlight } from 'react-native'
import AppLogo from '../component/AppLogo';
import { TextInput, Button } from 'react-native-paper';
import { color } from '../resource/color';
import { createTableUser } from '../action/appAction';

export default class RegisterScreen extends React.Component {
    state = {
        name : '',
        mobile : '',
        password : ''
    }

    handleSubmit() {
        var name = this.state.name;
        var mobile = this.state.mobile;
        var password = this.state.password;
        
        if(name == ''){
            alert("Enter name")
        }else if(mobile == ''){
            alert("Enter mobile number")
        }else if(mobile.length <10){
            alert("Mobile number should be 10 digit.")
        } else if(password == ''){
            alert("Enter password")
        }else{
           createTableUser(name,mobile,password,this.props.navigation.navigate)
           this.setState({
               name : '',
               mobile : '',
               password : ''
           })
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.logo}>
                    <AppLogo />
                </View>
                <View style={styles.heading}>
                    <Text style={styles.headingTxt}>Register to create your account</Text>
                </View>
                <View style={styles.form}>
                    <TextInput
                        label="Name"
                        mode='outlined'
                        onChangeText={(name) => this.setState({ name })}
                        style={styles.textbox}
                        value={this.state.name}
                    />
                    <TextInput
                        label="Mobile"
                        mode='outlined'
                        keyboardType="number-pad"
                        onChangeText={(mobile) => this.setState({ mobile })}
                        style={styles.textbox}
                        value={this.state.mobile}
                        maxLength={10}
                    />
                    <TextInput
                        label="Password"
                        mode='outlined'
                        secureTextEntry
                        onChangeText={(password) => this.setState({ password })}
                        style={styles.textbox}
                        value={this.state.password}
                    />
                    <Button
                        mode="contained"
                        style={styles.button}
                        onPress={() => this.handleSubmit()}>
                        <Text style={styles.btntxt}>Register</Text>
                    </Button>
                </View>
                <View style={styles.signContainer}>
                    <Text style={styles.signtxt}>Already register ? </Text>
                    <TouchableHighlight
                        onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={styles.signtxt1}>Login</Text>
                    </TouchableHighlight>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent:'center',
        // alignItems:'center'
    },
    logo: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    heading: {
        marginHorizontal: 10,
        marginTop: 20,
        marginBottom: 40
    },
    headingTxt: {
        fontSize: 20,
        fontWeight: "700"
    },
    form: {

    },
    textbox: {
        width: Dimensions.get('window').width - 30,
        marginHorizontal: 10,
        marginVertical: 10,
        borderColor: color.theme
    },
    button: {
        width: Dimensions.get('window').width - 20,
        height: 50,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingTop: 6,
        backgroundColor: color.theme
    },
    btntxt: {
        fontSize: 18,
        fontWeight: "700",
        borderRadius: 5,
        color: color.primery
    },
    signContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    signtxt: {
        fontSize: 18,
        //textAlign : 'center'
    },
    signtxt1: {
        fontSize: 18,
        color: color.theme
    }
})