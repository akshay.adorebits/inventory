import { openDatabase } from 'react-native-sqlite-storage';
import { View, StyleSheet, Button, Alert } from "react-native";

dbCreate = () => {
    const db = openDatabase({ name: 'UserDatabase.db' });
    /* user table */
    db.transaction((txn)=>{
        txn.executeSql(
            "CREATE TABLE IF NOT EXISTS user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20), mobile INT(10) UNIQUE, password VARCHAR(255))"
            ,[])
        console.log("Table Created")
       // txn.executeSql('DROP TABLE IF EXISTS user', []);
    })

    /* product table */
    db.transaction((txn)=>{
        txn.executeSql(
            "CREATE TABLE IF NOT EXISTS product(pro_id INTEGER PRIMARY KEY AUTOINCREMENT, pro_name VARCHAR(20), qty INT(10) , price INT(10) , image TEXT)"
            ,[])
        console.log("Table Created")
       // txn.executeSql('DROP TABLE IF EXISTS user', []);
    })

    /* order table */
    db.transaction((txn)=>{
        txn.executeSql(
            "CREATE TABLE IF NOT EXISTS cart(order_id INTEGER PRIMARY KEY AUTOINCREMENT, pro_id INT(10) ,pro_name VARCHAR(20), qty INT(10) , price INT(10), user_id INT(10), user_name VARCHAR(20), mobile INT(10), status INT(5))"
            ,[])
        console.log("Table Created")
       // txn.executeSql('DROP TABLE IF EXISTS user', []);
    })
    return db;
}

export function createTableUser (name,mobile,password,navigation) {
    const db = this.dbCreate();
    
    db.transaction((txn)=>{
        txn.executeSql("SELECT * FROM user WHERE mobile='"+mobile+"' ",[],(tx,result)=>{
            console.log(result.rows)
            var len = result.rows.length;
            if(len > 0){
                console.log("response",result.rows.item(0))
                alert("Mobile number already register");
                return;
            }
        })
    })

     db.transaction((txn)=>{
        txn.executeSql("INSERT INTO user(name, mobile,password) VALUES ('"+name+"','"+mobile+"','"+password+"')",[],(tx,result)=>{
            if(result.rowsAffected > 0){
                console.log("response",result)
              
               Alert.alert(
                'Success',
                'You are Registered Successfully',
                [
                  {
                    text: 'Ok',
                    onPress: () => navigation('Login'),
                  },
                ],
                { cancelable: false }
              );
            }
        })
        
    })
}

export function loginUser(mobile,password){
    const db = this.dbCreate();
   
    db.transaction((txn)=>{
        txn.executeSql("SELECT * FROM user WHERE mobile='"+mobile+"' AND password='"+password+"' ",[],(tx,result)=>{
            console.log(result.rows)
            var len = result.rows.length;
            if(len > 0){
                console.log("response",result.rows.item(0))
                //return result.rows.item(0);
                this.userData(result.rows.item(0));
            }
        })
        
    })
}

userData = async(res) => {
    // console.log(res.name)
    // AsyncStorage.setItem({name : JSON.stringify(res.name)})
    // AsyncStorage.setItem({mobile : JSON.stringify(res.mobile)})
    // AsyncStorage.setItem({user_id : JSON.stringify(res.user_id)})   
}

export function createTableProduct (pro_name,qty,price,image) {
    const db = this.dbCreate();

     db.transaction((txn)=>{
        txn.executeSql("INSERT INTO product(pro_name, qty, price, image) VALUES ('"+pro_name+"','"+qty+"','"+price+"','"+image+"')",[],(tx,result)=>{
            if(result.rowsAffected > 0){
                console.log("response",result)
               // return result;
                alert("Product add Successfully")
            }
        })
        
    })
}

export function updateProduct(pro_id,pro_name,qty,price,image,navigation){
    const db = this.dbCreate();

    db.transaction((txn)=>{
        txn.executeSql("UPDATE product SET pro_name = '"+pro_name+"', qty = '"+qty+"', price='"+price+"', image = '"+image+"' WHERE pro_id='"+pro_id+"'",[],(tx,result)=>{
            if(result.rowsAffected > 0){
                console.log("response",result)
               // return result;
                alert("Product update Successfully")
                navigation.navigate('Home');
            }
        }) 
    })
}

export function Order(pro_id,pro_name,qty,price,user_id,user_name,mobile,navigation){

    const db = this.dbCreate();
    db.transaction((txn)=>{
        txn.executeSql("SELECT * FROM cart where pro_id = '"+pro_id+"' AND status='0' AND user_id = '"+user_id+"'", [], (tx, result) => {
            
            var data = result.rows.item(0);
            
            if(data){
                alert("Product Already in cart");
            }else{
                txn.executeSql("INSERT INTO cart (pro_id,pro_name,qty,price,user_id,user_name,mobile,status) VALUES ('"+pro_id+"','"+pro_name+"','"+qty+"','"+price+"','"+user_id+"','"+user_name+"', '"+mobile+"','0')",[],(tx,result)=>{
                        
                    if(result.rowsAffected > 0){
                        console.log("response data",result)
                           
                        Alert.alert(
                                'Success',
                                'Product add in cart Successfully',
                                [
                                {
                                    text: 'Ok',
                                    onPress : () => navigation.navigate('Cart')
                                },
                                ],
                                { cancelable: false }
                            );
                        }
                })
                txn.executeSql("UPDATE product SET qty=(SELECT qty from product WHERE pro_id = '"+pro_id+"')-'"+qty+"' WHERE pro_id = '"+pro_id+"'",[],(tx,result)=>{
                   
                    if(result.rowsAffected > 0){
                        console.log("response",result)
                        
                    }
                })
            }

        })
 
    })
}

export function acceptOrder(id){
    const db = this.dbCreate();

    db.transaction((txn)=>{
        txn.executeSql("UPDATE cart SET status='1' WHERE order_id = '"+id+"'",[],(tx,result)=>{
            console.log(result)
            if(result.rowsAffected > 0){
                console.log("response",result)
               alert("Order Accepted")
            }
        })
    })
}

export function cancleOrder(id,pro_id){
    const db = this.dbCreate();

    db.transaction((txn)=>{
        txn.executeSql("UPDATE cart SET status='3' WHERE order_id = '"+id+"'",[],(tx,result)=>{
            console.log(result)
            if(result.rowsAffected > 0){
                console.log("response",result)
               alert("Order Cancel")
            }
        })

        txn.executeSql("UPDATE product SET qty=(SELECT qty from product WHERE pro_id = '"+pro_id+"')+(SELECT qty from cart where order_id='"+id+"') WHERE pro_id = '"+pro_id+"'",[],(tx,result)=>{
            console.log(result)
            if(result.rowsAffected > 0){
                console.log("response",result)
               
            }
        })
        
    })
}

export function deleteProduct(id,navigation){
    const db = this.dbCreate();

    db.transaction((txn)=>{
        txn.executeSql("DELETE FROM product WHERE pro_id = '"+id+"'",[],(tx,result)=>{
            console.log(result)
            if(result.rowsAffected > 0){
                console.log("response",result)
              
            }
        })
    })
}

export function deleteCart(pro_id,order_id){
    const db = this.dbCreate();

    db.transaction((txn)=>{ 

        txn.executeSql("UPDATE product SET qty=(SELECT qty from product WHERE pro_id = '"+pro_id+"')+(SELECT qty from cart where order_id='"+order_id+"') WHERE pro_id = '"+pro_id+"'",[],(tx,result)=>{
            console.log(result)
            if(result.rowsAffected > 0){
                console.log("response",result)
               
            }
        })
        
        txn.executeSql("delete from cart WHERE order_id = '"+order_id+"'",[],(tx,result)=>{
                console.log(result)
                if(result.rowsAffected > 0){
                    console.log("response",result)
                   //alert("Order Cancel")
                }
        })
    })
    
}

export function updateCart(cartItems){
    const db = this.dbCreate();
    
    cartItems.map((item,index) => {
        db.transaction((txn)=>{ 

            txn.executeSql("SELECT * FROM cart where order_id = '"+item.order_id+"'", [], (tx, result) => {
                var temp = [];
                for (let i = 0; i < result.rows.length; ++i)
                

                //console.log("result data ======>",result.rows.item(i)['qty'])

                var org_qty = result.rows.item(i)['qty'];
                var new_qty = item.qty;
                var change_qty = new_qty - org_qty;

                txn.executeSql("UPDATE product SET qty=(SELECT qty from product WHERE pro_id = '"+item.pro_id+"')-'"+change_qty+"' WHERE pro_id = '"+item.pro_id+"'",[],(tx,result)=>{
                        
                        if(result.rowsAffected > 0){
                            console.log("response",result)
                        }
                        
                })
              
                console.log("Orignal qty ==>",org_qty)
                console.log("new qty ==>",new_qty)
                console.log("change qty ==>",change_qty);
            })

            txn.executeSql("UPDATE cart SET qty='"+item.qty+"' WHERE order_id = '"+item.order_id+"'",[],(tx,result)=>{
               
                if(result.rowsAffected > 0){
                    console.log("Cart update",result)
                   
                }
            })
            

        })
 
    })
    alert("Cart update successfully");
}