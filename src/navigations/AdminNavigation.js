import React from 'react';
import {Button,Image,TouchableOpacity} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {color} from '../resource/color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';  
//import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';  

import Home from '../screens/admin/HomeScreen';
import Order from '../screens/admin/orderScreen';
import Profile from '../screens/admin/ProfileScreen';
import AddProduct from '../screens/admin/AddProduct';
import ConOrder from '../screens/admin/ConfirmOrder';
import Logout from '../screens/Logout';
import AsyncStorage from '@react-native-community/async-storage';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const Product = () =>{
    return(
        <Stack.Navigator >
            <Stack.Screen
                name="Home"
                component={Home}
                options = {({ navigation }) => ({
                    headerTitle: "Home",
                    headerRight: () => (
                        // <Button
                        // onPress={() =>navigation.navigate('Product')}
                        // title="Add"
                        // color="blue"
                       // />
                       <TouchableOpacity onPress={()=>navigation.navigate('Product')}>
                           <Image source={require('../resource/image/icons/plus.png')} style={{width : 30, height:30,marginRight:10}}/>
                       </TouchableOpacity>
                    ),
                    
                    headerLeft:null
                })  
                }
            />
            <Stack.Screen name="Product"
                component={AddProduct}
                options={{
                    headerTitle: "Product",
                    
                }}/>
        </Stack.Navigator>
    )
}

const ProfileScreen = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Profile"
                component={Profile}
                options = {({ navigation }) => ({
                    headerTitle: "Profile",
                    headerRight: () => (
                        // <Button
                        // onPress={() => navigation.navigate('Logout')}
                        // title="logout"
                        // color="blue"
                        // />
                        <TouchableOpacity onPress={()=>navigation.navigate('Logout')}>
                           <Image source={require('../resource/image/icons/logout.jpg')} style={{width : 30, height:30,marginRight:10}}/>
                       </TouchableOpacity>
                    ),
                    headerLeft:null
                })  
                }
            />
            <Stack.Screen 
                name="Logout"
                component={Logout}/>
        </Stack.Navigator>
    )
}

const OrderScreen = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Order"
                component={Order}
                options = {({ navigation }) => ({
                    headerTitle: "Order",
                    headerLeft:null
                })  
                }
            />
            {/* <Stack.Screen 
                name="AuthNatigator"
                component={AuthNatigator}/> */}
        </Stack.Navigator>
    )
} 

const ConfirmOrder = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="ConOrder"
                component={ConOrder}
                options = {({ navigation }) => ({
                    headerTitle: "Confirm Order",
                    headerLeft:null
                })  
                }
            />
        </Stack.Navigator>
    )
}

const AdminNavigation = () => {
  return (
    <Tab.Navigator>
        <Tab.Screen name="Home" component={Product} 
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => (
                    <Image source={require('../resource/image/icons/Offers.png')} style={{width:20,height:20}}/>
                ),
              }}/>
        <Tab.Screen name="Order" component={OrderScreen} 
            options={{
                tabBarLabel: "Order",
                tabBarIcon: () => <Image source={require('../resource/image/icons/Truck.png')} style={{width:20,height:20}}/>,
            }}/>
        <Tab.Screen name="ConfirmOrder" component={ConfirmOrder} 
            options={{
                tabBarLabel: "Confirm Order",
                tabBarIcon: () => <Image source={require('../resource/image/icons/Truck.png')} style={{width:20,height:20}}/>,
            }}/>
        <Tab.Screen name="Profile" component={ProfileScreen} 
             options={{
                tabBarLabel: "Profile",
                tabBarIcon: () => <Image source={require('../resource/image/icons/Profile.png')} style={{width:20,height:20}}/>,
            }}/>
    </Tab.Navigator>
  );
};

export default AdminNavigation;