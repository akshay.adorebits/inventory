import React from 'react';
import {Button,Image,TouchableOpacity} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import  Ionicons  from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from '../screens/Client/ProductScreen';
import Order from '../screens/Client/OrderScreen';
import Profile from '../screens/admin/ProfileScreen';
import ProductDetail from '../screens/Client/ProductDetail';
import Cart from '../screens/Client/CartScreen';
import Logout from '../screens/Logout';
import AsyncStorage from '@react-native-community/async-storage';


const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const Product = () =>{
    return(
        <Stack.Navigator >
            <Stack.Screen
                name="Home"
                component={Home}
                options = {({ navigation }) => ({
                    headerTitle: "Home",
                    headerLeft:null
                })  
                }
            />
            <Stack.Screen 
                name="ProductDetail"
                component={ProductDetail}
                options = {({ navigation }) => ({
                    headerTitle: "Product Detail",
                })  
                }
            />
        </Stack.Navigator>
    )
}

const ProfileScreen = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Profile"
                component={Profile}
                options = {({ navigation }) => ({
                    headerTitle: "Profile",
                    headerRight: () => (
                        // <Button
                        // onPress={() => navigation.navigate('Logout')}
                        // title="logout"
                        // color="blue"
                        // />
                        <TouchableOpacity onPress={()=>navigation.navigate('Logout')}>
                           <Image source={require('../resource/image/icons/logout.jpg')} style={{width : 30, height:30,marginRight:10}}/>
                       </TouchableOpacity>
                    ),
                    headerLeft:null
                })  
                }
            />
            <Stack.Screen 
                name="Logout"
                component={Logout}/>
        </Stack.Navigator>
    )
}

const OrderScreen = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Order"
                component={Order}
                options = {({ navigation }) => ({
                    headerTitle: "My Account",
                    headerLeft:null
                })  
                }
            />
            {/* <Stack.Screen 
                name="AuthNatigator"
                component={AuthNatigator}/> */}
        </Stack.Navigator>
    )
} 

const CartScreen = () =>{
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Cart"
                component={Cart}
                options = {({ navigation }) => ({
                     headerTitle: "Shoping Cart",
                     headerLeft:null
                    
                })  
                }
            />
            {/* <Stack.Screen 
                name="AuthNatigator"
                component={AuthNatigator}/> */}
        </Stack.Navigator>
    )
}

const ClientNavigation = () => {
  return (
    <Tab.Navigator>
        <Tab.Screen name="Home" 
            component={Product}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => (
                  <Image source={require('../resource/image/icons/Offers.png')} style={{width:20,height:20}}/>
                ),
              }}
        />
        <Tab.Screen name="CartScreen" component={CartScreen} 
            options={{
                tabBarLabel: "Cart",
                tabBarIcon: () => <Image source={require('../resource/image/icons/Truck.png')} style={{width:20,height:20}}/>,
            }}/>
        <Tab.Screen name="Order" component={OrderScreen} 
            options={{
                tabBarLabel: "My Account",
                tabBarIcon: () => <Image source={require('../resource/image/icons/Truck.png')} style={{width:20,height:20}}/>,
            }}/>
        <Tab.Screen name="Profile" component={ProfileScreen} 
            options={{
                    tabBarLabel: "Profile",
                    tabBarIcon: () => <Image source={require('../resource/image/icons/Profile.png')} style={{width:20,height:20}}/>,
                }}/>
    </Tab.Navigator>
  );
};

export default ClientNavigation;