import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import Admin from './AdminNavigation';
import Client from './ClientNavigation';
import AuthNavigation from './AuthNavigation';

const Stack = createStackNavigator();

const RootNavigation = (isSignedIn) => {
  //console.log("Root nav",isSignedIn.user)
  // return (
  //   isSignedIn.user !== null ? 
  //     (
  //       <Stack.Navigator>
  //         <Stack.Screen name="Admin" component={Admin} 
  //           options={{ headerShown: false }}/>
  //       </Stack.Navigator>
  //     ) : (
  //       <Stack.Navigator>
  //         <Stack.Screen name="AuthNavigation" component={AuthNavigation}
  //           options={{ headerShown: false }} />
  //       </Stack.Navigator>
  //     )
  // )

    if(isSignedIn.user !== null){
      
      if(isSignedIn.user == '1'){
       // console.log("Root Navigation Admin",isSignedIn.user)
        return (
          <Stack.Navigator>
            <Stack.Screen name="Admin" component={Admin} 
              options={{ headerShown: false }}/>
          </Stack.Navigator>
        ) 
      }
      if(isSignedIn.user != '1'){
        //console.log("Root Navigation Client",isSignedIn.user)
        return (
          <Stack.Navigator>
            <Stack.Screen name="Client" component={Client} 
              options={{ headerShown: false }}/>
          </Stack.Navigator>
        ) 
      }
    }else {
      return(
        <Stack.Navigator>
          <Stack.Screen name="AuthNavigation" component={AuthNavigation}
                  options={{ headerShown: false }} />
            </Stack.Navigator>
      )
    }
};

export default RootNavigation;