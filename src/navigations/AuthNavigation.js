import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../screens/LoginScreen';
import register from '../screens/RegisterScreen';
import Admin from './AdminNavigation';
import Client from './ClientNavigation';

const Stack = createStackNavigator();

const AuthNavigation = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Register" 
            component={register} 
            options={{ headerShown: false }}
        />
        <Stack.Screen name="Admin" 
            component={Admin} 
            options={{ headerShown: false }}
        />
        <Stack.Screen name="Client" 
            component={Client} 
            options={{ headerShown: false }}
        />
      </Stack.Navigator>
  );
};

export default AuthNavigation;